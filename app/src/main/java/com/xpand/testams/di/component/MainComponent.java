package com.xpand.testams.di.component;

import com.jess.arms.di.component.AppComponent;
import com.jess.arms.di.scope.ActivityScope;
import com.xpand.testams.di.module.MainModule;
import com.xpand.testams.mvp.ui.activity.MainActivity;

import dagger.Component;

/**
 * @author: Laitianbing
 * @date: 2017/6/2 14:39
 * @desc:
 */
@ActivityScope
@Component(modules = MainModule.class,dependencies = AppComponent.class)
public interface MainComponent {
    void inject(MainActivity activity);
}
