package com.xpand.testams.di.module;

import android.content.Context;

import com.jess.arms.di.scope.ActivityScope;
import com.xpand.testams.app.utils.BaiduUtils;
import com.xpand.testams.mvp.contract.MainContract;
import com.xpand.testams.mvp.model.MainModel;

import dagger.Module;
import dagger.Provides;

/**
 * @author: Laitianbing
 * @date: 2017/6/1 16:08
 * @desc:
 */
@Module
public class MainModule {
    private MainContract.View mView;

    public MainModule(MainContract.View view) {
        mView = view;
    }
    @ActivityScope
    @Provides
    MainContract.View provideView(){
        return mView;
    }

    @ActivityScope
    @Provides
    MainContract.Model provideModel(MainModel model){
        return model;
    }

}
