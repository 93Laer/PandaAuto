package com.xpand.testams.mvp.model;

import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.integration.IRepositoryManager;
import com.jess.arms.mvp.BaseModel;
import com.xpand.testams.mvp.contract.MainContract;
import com.xpand.testams.mvp.model.api.service.ApiService;
import com.xpand.testams.mvp.model.entity.LaerStation;
import com.xpand.testams.mvp.model.entity.NetRequestBody;
import com.xpand.testams.mvp.model.entity.ReqBodyUpdateStationInfo;
import com.xpand.testams.mvp.model.entity.ResBodyUpdateStationInfo;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * @author: Laitianbing
 * @date: 2017/6/1 16:12
 * @desc:
 */
@ActivityScope
public class MainModel extends BaseModel implements MainContract.Model {
    @Inject
    public MainModel(IRepositoryManager repositoryManager) {
        super(repositoryManager);
    }


    @Override
    public Observable<LaerStation> updateStationInfo(@Body ReqBodyUpdateStationInfo request) {

        String gson = request.getGson();

        return mRepositoryManager.obtainRetrofitService(ApiService.class)
                .updateStationInfo(request);
    }
}
