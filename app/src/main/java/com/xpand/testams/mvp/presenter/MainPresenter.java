package com.xpand.testams.mvp.presenter;

import android.graphics.Bitmap;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.MapPoi;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.model.LatLng;
import com.jess.arms.di.scope.ActivityScope;
import com.jess.arms.mvp.BasePresenter;
import com.xpand.testams.app.utils.BaiduUtils;
import com.xpand.testams.app.utils.RxUtils;
import com.xpand.testams.mvp.contract.MainContract;
import com.xpand.testams.mvp.model.entity.LaerStation;
import com.xpand.testams.mvp.model.entity.ReqBodyUpdateStationInfo;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import me.jessyan.rxerrorhandler.core.RxErrorHandler;
import me.jessyan.rxerrorhandler.handler.ErrorHandleSubscriber;
import me.jessyan.rxerrorhandler.handler.RetryWithDelay;

/**
 * @author: Laitianbing
 * @date: 2017/6/1 16:01
 * @desc:
 */
@ActivityScope
public class MainPresenter extends BasePresenter<MainContract.Model, MainContract.View> implements BaiduMap.OnMapStatusChangeListener, BaiduMap.OnMarkerClickListener, BaiduMap.OnMapClickListener {
    private RxErrorHandler mErrorHandler;
    private Bitmap mBitmap;
    private List<Marker> mMarkers = new ArrayList<>();
    private List<LaerStation.BodyBean.StationBean> mStations;

    @Inject
    public MainPresenter(MainContract.Model model, MainContract.View rootView, RxErrorHandler handler) {
        super(model, rootView);
        this.mErrorHandler = handler;
    }

    public void initMap(MapView mapView) {
        BaiduUtils.initMap(mapView);
        BaiduMap baiduMap = mapView.getMap();
        BaiduUtils.setMapOnClickListener(baiduMap, this);
        BaiduUtils.setOnMarkerClickListener(baiduMap, this);
        BaiduUtils.setOnMapStatusChangeListener(baiduMap, this);
        mRootView.updateMapStatus(new LatLng(30.591825,104.063292),14);
    }

    public void getStations() {
        // TODO: 2017/6/2 处理请求数据，并创建marker
        ReqBodyUpdateStationInfo.BodyBean bodyBean = new ReqBodyUpdateStationInfo.BodyBean();
        bodyBean.setCityid("610000")
                .setLongitude("104.063292")
                .setLatitude("30.591825")
                .setStation("1")
                .setPresetstation("0")
                .setChargingpile("0")
                .setPresetchargingpile("0");

        ReqBodyUpdateStationInfo reqBody = new ReqBodyUpdateStationInfo();
        reqBody.setHeader(new ReqBodyUpdateStationInfo.HeaderBean("getstations"))
                .setBody(bodyBean);
        mModel.updateStationInfo(reqBody)
                .subscribeOn(Schedulers.io())
                .retryWhen(new RetryWithDelay(3, 2))//遇到错误时重试,第一个参数为重试几次,第二个参数为重试的间隔
                .subscribeOn(AndroidSchedulers.mainThread())
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.bindToLifecycle(mRootView))//使用RXlifecycle,使subscription和activity一起销毁
                .subscribe(new ErrorHandleSubscriber<LaerStation>(mErrorHandler) {
                    @Override
                    public void onNext(@NonNull LaerStation laerStation) {
                        if (laerStation.getBody().getStation().size() != 0){
                            mStations = laerStation.getBody().getStation();
                            addAllMarker();
                        }
                    }
                });

    }


    @Override
    public void onMapStatusChangeStart(MapStatus mapStatus) {

    }

    @Override
    public void onMapStatusChange(MapStatus mapStatus) {

    }

    /**
     * 地图状态改变完成
     *
     * @param mapStatus
     */
    @Override
    public void onMapStatusChangeFinish(MapStatus mapStatus) {

    }

    /**
     * maker点击监听
     *
     * @param marker
     * @return
     */
    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    /**
     * 地图点击监听
     *
     * @param latLng
     */
    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMapPoiClick(MapPoi mapPoi) {
        return false;
    }

    private void addAllMarker() {
        MarkerOptions options;
        LatLng latLng;
        for (LaerStation.BodyBean.StationBean station : mStations) {
            latLng = new LatLng(station.getLatitude(), station.getLongtitude());
            BitmapDescriptor bitmapMarker = BitmapDescriptorFactory
                    .fromBitmap(mBitmap);
            //构建MarkerOption，用于在地图上添加Marker
            options = new MarkerOptions()
                    .position(latLng)
                    .icon(bitmapMarker);

            Marker marker = mRootView.addMarker(options);
            mMarkers.add(marker);
        }
    }

}
