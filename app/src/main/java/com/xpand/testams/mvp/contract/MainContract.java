package com.xpand.testams.mvp.contract;

import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.model.LatLng;
import com.jess.arms.mvp.IModel;
import com.jess.arms.mvp.IView;
import com.xpand.testams.mvp.model.entity.LaerStation;
import com.xpand.testams.mvp.model.entity.ReqBodyUpdateStationInfo;
import com.xpand.testams.mvp.model.entity.ResBodyUpdateStationInfo;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;

/**
 * @author: Laitianbing
 * @date: 2017/6/1 16:01
 * @desc:
 */

public interface MainContract {
    interface View extends IView {
        //更新地图的状态
        void updateMapStatus(LatLng latLng,  int zoom);
        //添加Mark,这里在p层中创建marker，
        Marker addMarker(MarkerOptions options);
        //显示当前的进度弹窗
        void showFragment();
        //获取当前订单进行的状态
        void setCurrentStatus(int status);
        //获取mapview
        MapView getMapView();
        //移除marker
        void removeMarker(Marker marker);

    }

    interface Model extends IModel {
        Observable<LaerStation> updateStationInfo(@Body ReqBodyUpdateStationInfo request);

    }
}
