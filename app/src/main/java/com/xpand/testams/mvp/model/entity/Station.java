package com.xpand.testams.mvp.model.entity;

import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: Laitianbing
 * @date: 2017/6/1 18:04
 * @desc:
 */

public class Station implements Serializable {
    /**
     * 站点ID
     */
    public String id;
    /**
     * 站点名称
     */
    public String stationname;
    /**
     * 站点地址
     */
    public String stationaddress;
    /**
     * 站点经度
     */
    public double longtitude;
    /**
     * 站点纬度
     */
    public double latitude;
    /**
     * 站点车辆数
     */
    public int vehiclenum = 0;
    /**
     * 站点充电桩数
     */
    public int pilenum;
    /**
     * 站点停车位
     */
    public int parkingnum;
    /**
     * 站点
     */
    public int avlvehiclenum = 0;
    /**
     * 站点可用车辆数
     */
    public int canusenum = 0;
    /**
     * 是否是预设站点，0不是 1是预设网点
     */
    public String preset;

    public String areaid;

    public Station() {
    }

    public int getAvlvehiclenum() {
        return avlvehiclenum;
    }

    public void setAvlvehiclenum(int avlvehiclenum) {
        this.avlvehiclenum = avlvehiclenum;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStationname() {
        return stationname;
    }

    public void setStationname(String stationname) {
        this.stationname = stationname;
    }

    public String getStationaddress() {
        return stationaddress;
    }

    public void setStationaddress(String stationaddress) {
        this.stationaddress = stationaddress;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public int getVehiclenum() {
        return vehiclenum;
    }

    public void setVehiclenum(int vehiclenum) {
        this.vehiclenum = vehiclenum;
    }

    public int getPilenum() {
        return pilenum;
    }

    public void setPilenum(int pilenum) {
        this.pilenum = pilenum;
    }

    public int getParkingnum() {
        return parkingnum;
    }

    public void setParkingnum(int parkingnum) {
        this.parkingnum = parkingnum;
    }

    public String getPreset() {
        return preset;
    }

    public void setPreset(String preset) {
        this.preset = preset;
    }

    public String getAreaid() {
        return areaid;
    }

    public void setAreaid(String areaid) {
        this.areaid = areaid;
    }



    public int getCanusenum() {
        return canusenum;
    }

    public void setCanusenum(int canusenum) {
        this.canusenum = canusenum;
    }

    @Override
    public String toString() {
        return "Station{" +
                "id='" + id + '\'' +
                ", stationname='" + stationname + '\'' +
                ", stationaddress='" + stationaddress + '\'' +
                ", longtitude=" + longtitude +
                ", latitude=" + latitude +
                ", vehiclenum=" + vehiclenum +
                ", pilenum=" + pilenum +
                ", parkingnum=" + parkingnum +
                ", preset='" + preset + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Station station = (Station) o;

        return id != null ? id.equals(station.id) : station.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}