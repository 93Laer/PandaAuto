package com.xpand.testams.mvp.model.api.service;

import com.xpand.testams.mvp.model.entity.LaerStation;
import com.xpand.testams.mvp.model.entity.ReqBodyUpdateStationInfo;
import com.xpand.testams.mvp.model.entity.ResBodyUpdateStationInfo;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * @author: Laitianbing
 * @date: 2017/5/27 11:38
 * @desc:
 */

public interface ApiService {

    /**
     * ***********************************
     * <p/>
     * 网点相关网络请求
     * 1. 更新网点信息
     * <p/>
     * *********************************
     */
    /**
     * 更新网点信息
     *
     * @param request
     * @return
     */
    @Headers({
            "Accept: application/json"
    })
    @POST("/api/app?version=" + "" + "&client=android&service=getstations")
    Observable<LaerStation> updateStationInfo(@Body ReqBodyUpdateStationInfo request);

}
