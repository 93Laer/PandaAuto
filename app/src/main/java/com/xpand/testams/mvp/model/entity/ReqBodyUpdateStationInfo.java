package com.xpand.testams.mvp.model.entity;

import com.google.gson.Gson;

/**
 * 分时租赁更新网点信息
 */

public class ReqBodyUpdateStationInfo extends NetRequestBody {

    /**
     * service : getstations
     */

    private HeaderBean header;
    /**
     * cityid : 400000
     * longitude : 104.063292
     * station : 1
     * presetstation : 0
     * latitude : 30.591825
     * chargingpile : 0
     * presetchargingpile : 0
     */

    private BodyBean body;


    public ReqBodyUpdateStationInfo setHeader(HeaderBean header) {
        this.header = header;
        return this;
    }


    public ReqBodyUpdateStationInfo setBody(BodyBean body) {
        this.body = body;
        return this;
    }

    public static class HeaderBean {
        private String service;

        public HeaderBean(String service) {
            this.service = service;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }
    }

    public static class BodyBean {
        private String cityid;
        private String longitude;
        private String station;
        private String presetstation;
        private String latitude;
        private String chargingpile;
        private String presetchargingpile;

        public String getCityid() {
            return cityid;
        }

        public BodyBean setCityid(String cityid) {
            this.cityid = cityid;
            return this;
        }

        public String getLongitude() {
            return longitude;
        }

        public BodyBean setLongitude(String longitude) {
            this.longitude = longitude;
            return this;
        }

        public String getStation() {
            return station;
        }

        public BodyBean setStation(String station) {
            this.station = station;
            return this;
        }

        public String getPresetstation() {
            return presetstation;
        }

        public BodyBean setPresetstation(String presetstation) {
            this.presetstation = presetstation;
            return this;
        }

        public String getLatitude() {
            return latitude;
        }

        public BodyBean setLatitude(String latitude) {
            this.latitude = latitude;
            return this;
        }

        public String getChargingpile() {
            return chargingpile;
        }

        public BodyBean setChargingpile(String chargingpile) {
            this.chargingpile = chargingpile;
            return this;
        }

        public String getPresetchargingpile() {
            return presetchargingpile;
        }

        public BodyBean setPresetchargingpile(String presetchargingpile) {
            this.presetchargingpile = presetchargingpile;
            return this;
        }
    }

    public String getGson() {
        return new Gson().toJson(this);
    }

}
