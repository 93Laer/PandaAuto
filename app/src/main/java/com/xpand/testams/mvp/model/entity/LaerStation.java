package com.xpand.testams.mvp.model.entity;

import java.util.List;

/**
 * @author: Laitianbing
 * @date: 2017/6/2 15:18
 * @desc:
 */

public class LaerStation {

    private BodyBean body;
    /**
     * service : getstations
     * errorcode : 1000
     */

    private HeaderBean header;

    public BodyBean getBody() {
        return body;
    }

    public void setBody(BodyBean body) {
        this.body = body;
    }

    public HeaderBean getHeader() {
        return header;
    }

    public void setHeader(HeaderBean header) {
        this.header = header;
    }

    public static class BodyBean {
        /**
         * areaname : 北碚区
         * stationaddress : 思源幼儿园正门右侧
         * pilenum : 0
         * stationname : 思源幼儿园
         * areaid : 22
         * avlvehiclenum : 0
         * othernum : 0
         * lowpowernum : 0
         * id : 49
         * citycode : 400000
         * idelnum : 0
         * longtitude : 106.562615
         * vehiclenum : 3
         * stationcode : SYYEY
         * parkingnum : 6
         * latitude : 29.815537
         * canusenum : 1
         */

        private List<StationBean> station;
        private List<?> presetstation;
        private List<?> presetchargingpile;
        private List<?> chargingpile;

        public List<StationBean> getStation() {
            return station;
        }

        public void setStation(List<StationBean> station) {
            this.station = station;
        }

        public List<?> getPresetstation() {
            return presetstation;
        }

        public void setPresetstation(List<?> presetstation) {
            this.presetstation = presetstation;
        }

        public List<?> getPresetchargingpile() {
            return presetchargingpile;
        }

        public void setPresetchargingpile(List<?> presetchargingpile) {
            this.presetchargingpile = presetchargingpile;
        }

        public List<?> getChargingpile() {
            return chargingpile;
        }

        public void setChargingpile(List<?> chargingpile) {
            this.chargingpile = chargingpile;
        }

        public static class StationBean {
            private String areaname;
            private String stationaddress;
            private int pilenum;
            private String stationname;
            private int areaid;
            private int avlvehiclenum;
            private int othernum;
            private int lowpowernum;
            private int id;
            private String citycode;
            private int idelnum;
            private double longtitude;
            private int vehiclenum;
            private String stationcode;
            private int parkingnum;
            private double latitude;
            private int canusenum;

            public String getAreaname() {
                return areaname;
            }

            public void setAreaname(String areaname) {
                this.areaname = areaname;
            }

            public String getStationaddress() {
                return stationaddress;
            }

            public void setStationaddress(String stationaddress) {
                this.stationaddress = stationaddress;
            }

            public int getPilenum() {
                return pilenum;
            }

            public void setPilenum(int pilenum) {
                this.pilenum = pilenum;
            }

            public String getStationname() {
                return stationname;
            }

            public void setStationname(String stationname) {
                this.stationname = stationname;
            }

            public int getAreaid() {
                return areaid;
            }

            public void setAreaid(int areaid) {
                this.areaid = areaid;
            }

            public int getAvlvehiclenum() {
                return avlvehiclenum;
            }

            public void setAvlvehiclenum(int avlvehiclenum) {
                this.avlvehiclenum = avlvehiclenum;
            }

            public int getOthernum() {
                return othernum;
            }

            public void setOthernum(int othernum) {
                this.othernum = othernum;
            }

            public int getLowpowernum() {
                return lowpowernum;
            }

            public void setLowpowernum(int lowpowernum) {
                this.lowpowernum = lowpowernum;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getCitycode() {
                return citycode;
            }

            public void setCitycode(String citycode) {
                this.citycode = citycode;
            }

            public int getIdelnum() {
                return idelnum;
            }

            public void setIdelnum(int idelnum) {
                this.idelnum = idelnum;
            }

            public double getLongtitude() {
                return longtitude;
            }

            public void setLongtitude(double longtitude) {
                this.longtitude = longtitude;
            }

            public int getVehiclenum() {
                return vehiclenum;
            }

            public void setVehiclenum(int vehiclenum) {
                this.vehiclenum = vehiclenum;
            }

            public String getStationcode() {
                return stationcode;
            }

            public void setStationcode(String stationcode) {
                this.stationcode = stationcode;
            }

            public int getParkingnum() {
                return parkingnum;
            }

            public void setParkingnum(int parkingnum) {
                this.parkingnum = parkingnum;
            }

            public double getLatitude() {
                return latitude;
            }

            public void setLatitude(double latitude) {
                this.latitude = latitude;
            }

            public int getCanusenum() {
                return canusenum;
            }

            public void setCanusenum(int canusenum) {
                this.canusenum = canusenum;
            }
        }
    }

    public static class HeaderBean {
        private String service;
        private String errorcode;

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getErrorcode() {
            return errorcode;
        }

        public void setErrorcode(String errorcode) {
            this.errorcode = errorcode;
        }
    }
}
