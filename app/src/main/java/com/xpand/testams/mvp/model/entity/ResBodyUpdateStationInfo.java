package com.xpand.testams.mvp.model.entity;

import java.util.List;

public class ResBodyUpdateStationInfo {

    private List<StationInfo> station;


    public List<StationInfo> getStation() {
        return station;
    }

    public void setStation(List<StationInfo> station) {
        this.station = station;
    }
}