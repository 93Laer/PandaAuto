package com.xpand.testams.mvp.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.FrameLayout;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.model.LatLng;
import com.jess.arms.base.BaseActivity;
import com.jess.arms.di.component.AppComponent;
import com.jess.arms.utils.UiUtils;
import com.xpand.testams.R;
import com.xpand.testams.di.component.DaggerMainComponent;
import com.xpand.testams.di.module.MainModule;
import com.xpand.testams.mvp.contract.MainContract;
import com.xpand.testams.mvp.presenter.MainPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * @author: Laitianbing
 * @date: 2017/6/1 16:00
 * @desc:
 */

public class MainActivity extends BaseActivity<MainPresenter> implements MainContract.View {
    public static final int DEFAULT_STATUS = 0;//默认状态
    public static final int STATION_DETAIL = 1;//站点详情

    public static final int COMMIT_ORDER = 2;//提交订单
    public static final int CHANGE_CAR = 3;//换车

    public static final int PICK_UP_CAR = 4;//取车
    public static final int CONTROL_CAR = 5;//车辆控制
    public static final int PAY_MONEY = 6;//付款

    private int currentStatus;//当前的状态,，
    @Nullable
    @BindView(R.id.mapView)
     MapView mMapView;

    @Nullable
    @BindView(R.id.content)
     FrameLayout mContent;

    private BaiduMap mBaiduMap;

    @Override
    public void setupActivityComponent(AppComponent appComponent) {
        DaggerMainComponent.builder()
                .appComponent(appComponent)
                .mainModule(new MainModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int initView(Bundle savedInstanceState) {
        return R.layout.map_layout;
    }

    @Override
    public void initData(Bundle savedInstanceState) {
        mBaiduMap = mMapView.getMap();
        mPresenter.initMap(mMapView);
        mPresenter.getStations();
    }

    @Override
    public void showLoading() {

    }

    @Override
    public void hideLoading() {

    }

    @Override
    public void showMessage(String message) {
        UiUtils.SnackbarText(message);
    }

    @Override
    public void launchActivity(Intent intent) {

    }

    @Override
    public void killMyself() {

    }


    /**
     * @param latLng
     * @param zoom   当zoom<=0，这时代表不进行地图的缩放
     */
    @Override
    public void updateMapStatus(LatLng latLng, int zoom) {
        if (zoom <= 0) {
            mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(latLng).build()));
        } else {
            mBaiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(latLng).zoom(zoom).build()));
        }
    }

    @Override
    public Marker addMarker(MarkerOptions options) {
        return (Marker) mBaiduMap.addOverlay(options);
    }


    @Override
    public void showFragment() {

    }

    @Override
    public void setCurrentStatus(int status) {
        currentStatus = status;
        showFragment();
    }

    @Override
    public MapView getMapView() {
        return mMapView;
    }

    @Override
    public void removeMarker(Marker marker) {

    }


}
