package com.xpand.testams.mvp.model.entity;

import java.io.Serializable;

public class StationInfo implements Serializable {


    //站点id
    long ssid;

    String stationname;

    String stationaddress;

    //    站点经纬度
    double longtitude;
    double latitude;

    //    可用车辆数，不稳定
    int canusenum;

    //城市id
    String cityid;

    //辖区id
    long areaid;

    //操作说明
    String update_flag;


    public long getAreaid() {
        return areaid;
    }

    public void setAreaid(long areaid) {
        this.areaid = areaid;
    }

    public int getCanusenum() {
        return canusenum;
    }

    public void setCanusenum(int canusenum) {
        this.canusenum = canusenum;
    }

    public String getCityid() {
        return cityid;
    }

    public void setCityid(String cityid) {
        this.cityid = cityid;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public long getSsid() {
        return ssid;
    }

    public void setSsid(long ssid) {
        this.ssid = ssid;
    }

    public String getStationaddress() {
        return stationaddress;
    }

    public void setStationaddress(String stationaddress) {
        this.stationaddress = stationaddress;
    }

    public String getStationname() {
        return stationname;
    }

    public void setStationname(String stationname) {
        this.stationname = stationname;
    }

    public String getUpdate_flag() {
        return update_flag;
    }

    public void setUpdate_flag(String update_flag) {
        this.update_flag = update_flag;
    }

    @Override
    public String toString() {
        return "StationInfo{" +
                ", ssid=" + ssid +
                '}';
    }
}