package com.xpand.testams.app;

import com.baidu.mapapi.SDKInitializer;
import com.jess.arms.base.BaseApplication;

/**
 * @author: Laitianbing
 * @date: 2017/6/1 15:53
 * @desc:
 */

public class App extends BaseApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        //在使用SDK各组件之前初始化context信息，传入ApplicationContext
        //注意该方法要再setContentView方法之前实现
        SDKInitializer.initialize(getApplicationContext());
    }
}
