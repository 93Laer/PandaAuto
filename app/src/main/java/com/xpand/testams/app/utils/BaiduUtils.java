package com.xpand.testams.app.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.ZoomControls;

import com.baidu.mapapi.map.BaiduMap;
import com.baidu.mapapi.map.BitmapDescriptor;
import com.baidu.mapapi.map.BitmapDescriptorFactory;
import com.baidu.mapapi.map.InfoWindow;
import com.baidu.mapapi.map.MapStatus;
import com.baidu.mapapi.map.MapStatusUpdateFactory;
import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.Marker;
import com.baidu.mapapi.map.MarkerOptions;
import com.baidu.mapapi.map.UiSettings;
import com.baidu.mapapi.model.LatLng;

/**
 * 地图处理工具类
 * Created by Laer on 2017/2/24.
 */
public class BaiduUtils {

    /**
     * 初始化地图
     *
     * @param mMapView
     * @return
     */
    public static BaiduMap initMap(MapView mMapView) {
        BaiduMap baiduMap = mMapView.getMap();
        baiduMap.setMapType(BaiduMap.MAP_TYPE_NORMAL);
        mMapView.showZoomControls(true);//开启手势放大控制
        UiSettings uiSettings = baiduMap.getUiSettings();
        uiSettings.setRotateGesturesEnabled(false);//禁止旋转
        uiSettings.setOverlookingGesturesEnabled(false);
        //隐藏百度地图Log
        View child = mMapView.getChildAt(1);
        if (child != null && (child instanceof ImageView || child instanceof ZoomControls)) {
            child.setVisibility(View.INVISIBLE);
        }
        return baiduMap;
    }

    public static void setMapOnClickListener(BaiduMap baiduMap,BaiduMap.OnMapClickListener listener) {
        baiduMap.setOnMapClickListener(listener);
    }

    public static  void setOnMarkerClickListener(BaiduMap baiduMap,BaiduMap.OnMarkerClickListener listener) {
        baiduMap.setOnMarkerClickListener(listener);
    }

    public static  void setOnMapStatusChangeListener(BaiduMap baiduMap,BaiduMap.OnMapStatusChangeListener listener) {
        baiduMap.setOnMapStatusChangeListener(listener);
    }

    /**
     * 更新地图的状态，并更新地图层级
     *
     * @param latLng
     * @param zoom   new LatLng(30.706072, 103.995883)
     */
    public static  void updateMapStatus(BaiduMap baiduMap,LatLng latLng, int zoom) {
        baiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(latLng).zoom(zoom).build()));
    }

    /**
     * 更新地图的状态，不改变地图的级别
     *
     * @param latLng new LatLng(30.706072, 103.995883)
     */
    public static  void updateMapStatus(BaiduMap baiduMap,LatLng latLng) {
        baiduMap.animateMapStatus(MapStatusUpdateFactory.newMapStatus(new MapStatus.Builder().target(latLng).build()));
    }

    /**
     * 创建一个InfoWindow
     *
     * @param view
     * @param latLng
     * @param offset 偏移量
     */
    public static  void creatInfoWindow(BaiduMap baiduMap,View view, LatLng latLng, int offset) {
        //创建InfoWindow展示的view
//定义用于显示该InfoWindow的坐标点
//创建InfoWindow , 传入 view， 地理坐标， y 轴偏移量
        InfoWindow mInfoWindow = new InfoWindow(view, latLng, offset);
//显示InfoWindow
        baiduMap.showInfoWindow(mInfoWindow);
    }

    /**
     * 创建一个InfoWindow
     *
     * @param viewRes
     * @param latLng
     * @param offset  偏移量
     */
    public static  void creatInfoWindow(Context context,BaiduMap baiduMap,int viewRes, LatLng latLng, int offset) {
        //创建InfoWindow展示的view
//定义用于显示该InfoWindow的坐标点
//创建InfoWindow , 传入 view， 地理坐标， y 轴偏移量
        InfoWindow mInfoWindow = new InfoWindow(LayoutInflater.from(context).inflate(viewRes, null), latLng, offset);
//显示InfoWindow
        baiduMap.showInfoWindow(mInfoWindow);
    }

    /**
     * 添加一个marker，显示bitmap的形式添加
     *
     * @param latLng
     * @param bitmap
     * @param title  marker的title
     */
    public  static void addMarker(BaiduMap baiduMap,LatLng latLng, Bitmap bitmap, String title) {
        BitmapDescriptor bitmapMarker = BitmapDescriptorFactory
                .fromBitmap(bitmap);
        //构建MarkerOption，用于在地图上添加Marker
        final MarkerOptions option = new MarkerOptions()
                .position(latLng)
                .icon(bitmapMarker);
        //        options.add(option);
        //在地图上添加Marker，并显示
        Marker marker = (Marker) baiduMap.addOverlay(option);
        marker.setTitle(title);
    }

    /**
     * 以展示一个布局（view）的方式，显示marker
     *
     * @param latLng
     * @param view
     * @param title
     */
    public static  void addMarkerLayer(BaiduMap baiduMap,LatLng latLng, View view, String title) {
        MarkerOptions options = new MarkerOptions();
        BitmapDescriptor bitmapDescriptor =
                BitmapDescriptorFactory.fromView(view);
        options.icon(bitmapDescriptor);
//        Bundle bundle = new Bundle();
//        bundle.putString("content", "这是一个通过options里面传递过去的信息");
        options.position(latLng)
                .title(title);
//                .extraInfo(bundle)
//                .rotate(360f).draggable(true).period(10);
        baiduMap.addOverlay(options);
    }

    /**
     * 以展示一个布局（view）的方式，显示marker
     *
     * @param latLng
     * @param viewRes
     * @param title
     */
    public  static void addMarkerLayer(Context context,BaiduMap baiduMap,LatLng latLng, int viewRes, String title) {
        MarkerOptions options = new MarkerOptions();
        BitmapDescriptor bitmapDescriptor =
                BitmapDescriptorFactory.fromView(LayoutInflater.from(context).inflate(viewRes, null));
        options.icon(bitmapDescriptor);
//        Bundle bundle = new Bundle();
//        bundle.putString("content", "这是一个通过options里面传递过去的信息");
        options.position(latLng)
                .title(title);
//                .extraInfo(bundle)
//                .rotate(360f).draggable(true).period(10);
        baiduMap.addOverlay(options);
    }
}