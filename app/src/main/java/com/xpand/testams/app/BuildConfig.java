package com.xpand.testams.app;

public final class BuildConfig {
    public static final boolean DEBUG = false;
    public static final String APPLICATION_ID = "com.xpand.arms";
    public static final String BUILD_TYPE = "release";
    public static final String FLAVOR = "";
    public static final int VERSION_CODE = 103;
    public static final String VERSION_NAME = "2.0.3";
    // Fields from build type: debug
    public static final boolean LOG_DEBUG = true;
    public static final boolean USE_CANARY = true;
    public BuildConfig() {
    }
}